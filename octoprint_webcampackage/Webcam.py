import os
from subprocess import call
from io import open  # for Python 2 & 3
from . import constants
try:
    from ruamel.yaml import YAML
except ImportError:
    from ruamel.yaml.main import YAML
yaml = YAML(typ="safe")


class Webcam():
    def __init__(self, plugin):
        self._logger = plugin._logger
        self._settings = plugin._settings
        self._plugin_manager = plugin._plugin_manager
        self._identifier = plugin._identifier
        self.installed = False
        self.isHubS = False

    def determineHubVersion(self):
        hub_file_path = os.path.expanduser('~') + "/.mosaicdata/canvas-hub-data.yml"

        if os.path.exists(hub_file_path):
            hub_data = open(hub_file_path, "r")
            hub_yaml = yaml.load(hub_data)
            hub_data.close()

            hub_rank = hub_yaml["versions"]["global"]
            if hub_rank == "0.2.0":
                return True
        return False

    def checkWebcamFlag(self):
        return os.path.isfile(constants.WEBCAM_FLAG_PATH)

    def updateUI(self, data):
        self._logger.info("Updating UI: %s" % data["command"])
        self._plugin_manager.send_plugin_message(self._identifier, data)

    def updateUIAll(self):
        self._logger.info("Updating all UI variables")
        self.updateUI({"command": "installed", "status": self.installed})
        self.updateUI({"command": "isHubS", "status": self.isHubS})
        self.updateUI({"command": "selectedResolution", "status": self._settings.get(["selectedResolution"])})
        self.updateUI({"command": "frameRate", "status": self._settings.get(["frameRate"])})
        self.updateUI({"command": "quality", "status": self._settings.get(["quality"])})
        self.updateUI({"command": "yuv", "status": self._settings.get(["yuv"])})
        self.updateUI({"command": "manualConfig", "status": self._settings.get(["manualConfig"])})

    def installPackage(self):
        if self.isHubS and not self.installed:
            self._logger.info('Installing Webcam Package')
            self.updateUI({"command": "installationStart"})
            call(["sudo sh %s" % constants.INSTALL_SCRIPT_PATH], shell=True)
            self.updateUI({"command": "installationFinished"})
            self.installed = self.checkWebcamFlag()
            self.updateUI({"command": "installed", "status": self.installed})
        else:
            self._logger.info('Webcam already installed')

    def saveConfigSettings(self, data):
        if self.isHubS and self.installed:
            # save settings in octoprint
            self._settings.set(["selectedResolution"], data["selectedResolution"], force=True)
            self._settings.set(["frameRate"], data["frameRate"], force=True)
            self._settings.set(["quality"], data["quality"], force=True)
            self._settings.set(["yuv"], data["yuv"], force=True)
            self._settings.set(["manualConfig"], data["manualConfig"], force=True)
            self._settings.save(force=True)
            self._logger.info("Webcam config settings changed")

            # convert values into proper strings for config file
            config_options = ""

            if data["manualConfig"]:
                config_options = "MANUAL_OPTIONS=\"%s\"\n" % data["manualConfig"]
            else:
                resolution_config_value = "RESOLUTION=\"%s\"\n" % data["selectedResolution"]
                frame_rate_config_value = "FRAME_RATE=\"%s\"\n" % data["frameRate"]
                quality_config_value = "QUALITY=\"%s\"\n" % data["quality"]
                yuv = ""
                if data["yuv"]:
                    yuv = "true"
                else:
                    yuv = "false"
                yuv_config_value = "YUV=\"%s\"\n" % yuv

                config_options = config_options.join([
                    resolution_config_value,
                    frame_rate_config_value,
                    quality_config_value,
                    yuv_config_value
                ])

            # write to config file
            config_file = open(constants.WEBCAM_CONFIG_FILE_PATH, "w")
            config_file.write(config_options)
            config_file.close()
            self._logger.info("Webcam config file updated")

            # Reset webcam script and run it again
            call(["sudo %s stop" % constants.WEBCAM_SCRIPT_PATH], shell=True)
            call(["sudo %s start" % constants.WEBCAM_SCRIPT_PATH], shell=True)

            # update UI
            self.updateUI({"command": "settingsSaved"})
