# SOFTWARE UPDATE
GITLAB_PROJECT_ID = 13379186
LATEST_VERSION_URL = "https://gitlab.com/api/v4/projects/%s/releases" % GITLAB_PROJECT_ID
PLUGIN_UPDATE_URL = "https://gitlab.com/mosaic-mfg/webcam-package/-/archive/master/webcam-package-master.zip"

# WEBCAM FILES PATHS
WEBCAM_FLAG_PATH = "/home/pi/.mosaicdata/webcam_flag"
INSTALL_SCRIPT_PATH = "/home/pi/OctoPrint/venv/lib/python2.7/site-packages/octoprint_webcampackage/static/webcam_install.sh"
WEBCAM_CONFIG_FILE_PATH = "/home/pi/webcam/webcam_config"
WEBCAM_SCRIPT_PATH = "/home/pi/webcam/mjpg-streamer.sh"
