# coding=utf-8
from __future__ import absolute_import
from distutils.version import LooseVersion
import requests
import octoprint.plugin
from . import Webcam
from . import constants


class WebcamPlugin(octoprint.plugin.StartupPlugin,
                   octoprint.plugin.TemplatePlugin,
                   octoprint.plugin.SettingsPlugin,
                   octoprint.plugin.AssetPlugin,
                   octoprint.plugin.SimpleApiPlugin,
                   octoprint.plugin.EventHandlerPlugin,
                   ):

    # STARTUPPLUGIN
    def on_after_startup(self):
        self._logger.info("%s Plugin STARTED" % self._plugin_info)
        self.webcam = Webcam.Webcam(self)
        self.webcam.installed = self.webcam.checkWebcamFlag()
        self.webcam.isHubS = self.webcam.determineHubVersion()
        self.webcam.updateUIAll()

    # SETTINGSPLUGIN
    def get_settings_defaults(self):
        return dict(
            selectedResolution="1280x720",
            frameRate=10,
            quality=60,
            yuv=False,
            manualConfig=""
        )

    # TEMPLATEPLUGIN
    def get_template_configs(self):
        return [
            dict(type="navbar", custom_bindings=False)
        ]

    # ASSETPLUGIN
    def get_assets(self):
        return dict(
            css=["css/webcampackage.css"],
            js=["js/webcampackage.js", "js/utils/constants.js"],
            less=["less/webcampackage.less"]
        )

    # SIMPLEAPIPLUGIN POST, runs first before on_api_commands, responds to commands from palette,js, any strings inside array = mandatory
    def get_api_commands(self):
        return dict(
            installPackage=[],
            saveConfigSettings=["selectedResolution", "frameRate", "quality", "yuv", "manualConfig"]
        )

    # SIMPLEAPIPLUGIN POST, to handle commands listed in get_api_commands
    def on_api_command(self, command, data):
        if command == "installPackage":
            self.webcam.installPackage()
        elif command == "saveConfigSettings":
            self.webcam.saveConfigSettings(data)

    def on_event(self, event, payload):
        try:
            if "ClientOpened" in event:
                self.webcam.installed = self.webcam.checkWebcamFlag()
                self.webcam.updateUIAll()
        except Exception as e:
            self._logger.info(e)

    def get_latest(self, target, check, full_data=False, online=True):
        resp = requests.get(constants.LATEST_VERSION_URL)
        version_data = resp.json()
        version = version_data[0]["name"]
        current_version = check.get("current")
        information = dict(
            local=dict(
                name=current_version,
                value=current_version,
            ),
            remote=dict(
                name=version,
                value=version
            )
        )
        self._logger.info("current version: %s" % current_version)
        self._logger.info("remote version: %s" % version)
        needs_update = LooseVersion(current_version) < LooseVersion(version)
        self._logger.info("needs update: %s" % needs_update)

        return information, not needs_update

    def get_update_information(self):
        # Define the configuration for your plugin to use with the Software Update
        # Plugin here. See https://github.com/foosel/OctoPrint/wiki/Plugin:-Software-Update
        # for details.
        return dict(
            webcam=dict(
                displayName="Webcam Package",
                displayVersion=self._plugin_version,
                current=self._plugin_version,
                python_checker=self,
                type="python_checker",
                command="/home/pi/test-version.sh",

                # update method: pip
                pip=constants.PLUGIN_UPDATE_URL,
            )
        )


# If you want your plugin to be registered within OctoPrint under a different name than what you defined in setup.py
# ("OctoPrint-PluginSkeleton"), you may define that here. Same goes for the other metadata derived from setup.py that
# can be overwritten via __plugin_xyz__ control properties. See the documentation for that.
__plugin_pythoncompat__ = ">=2.7,<4"


def __plugin_load__():
    global __plugin_implementation__
    __plugin_implementation__ = WebcamPlugin()

    global __plugin_hooks__
    __plugin_hooks__ = {
        "octoprint.plugin.softwareupdate.check_config": __plugin_implementation__.get_update_information
    }
