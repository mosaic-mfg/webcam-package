#!/bin/sh

echo 'Webcam Installation Package: STARTED'

# turn off camera in raspi-config
echo 'Checking raspi-config camera'
if ! grep "start_x=0" /boot/config.txt; then
  echo 'Turning off raspi-config camera'

  echo '' >> /boot/config.txt
  echo '# Disable camera' >> /boot/config.txt
  echo 'start_x=0' >> /boot/config.txt
  echo 'gpu_mem=128' >> /boot/config.txt
fi

# install MJPEG-Streamer
echo 'Installing MJPEG-Streamer...'
apt-get update
apt-get upgrade -y
apt-get install build-essential libjpeg8-dev imagemagick libv4l-dev cmake -y

cd /tmp
git clone https://github.com/jacksonliam/mjpg-streamer.git
cd mjpg-streamer/mjpg-streamer-experimental

make
make install

# make MJPEG-Streamer
echo 'Installing MJPEG-Streamer auto-start on boot...'

mkdir /home/pi/webcam
cp /home/pi/OctoPrint/venv/lib/python2.7/site-packages/octoprint_webcampackage/static/mjpg-streamer.sh /home/pi/webcam/mjpg-streamer.sh
cp /home/pi/OctoPrint/venv/lib/python2.7/site-packages/octoprint_webcampackage/static/webcam_config /home/pi/webcam/webcam_config
chmod +x /home/pi/webcam/mjpg-streamer.sh
chmod 777 /home/pi/webcam/webcam_config
chown -R pi:pi /home/pi/webcam
echo '@reboot /home/pi/webcam/mjpg-streamer.sh start' >> /home/pi/new_crontab
crontab -u pi /home/pi/new_crontab
rm /home/pi/new_crontab

# install FFMpeg
echo 'Installing FFMpeg...'
apt-get update
apt-get install ffmpeg -y

# update octoprint config
echo 'Updating OctoPrint config...'
python /home/pi/OctoPrint/venv/lib/python2.7/site-packages/octoprint_webcampackage/static/webcam.py

# set success flag
touch /home/pi/.mosaicdata/webcam_flag
echo 'Webcam Installation Package: DONE'

# restart
# reboot