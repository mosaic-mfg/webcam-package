from subprocess import check_output, CalledProcessError
from ruamel.yaml import YAML
yaml = YAML(typ="safe")
yaml.default_flow_style = False

# get ip address
try:
    url = check_output(['hostname'])
except CalledProcessError as e:
    url = '127.0.0.1'

# modify octoprint config file
octoprint_config_file_path = '/home/pi/.octoprint/config.yaml'
octoprint_config_data = open(octoprint_config_file_path, "r")
octoprint_config_yaml = yaml.load(octoprint_config_data)
octoprint_config_data.close()

ffpmeg_value = '/usr/bin/ffmpeg'
snapshot_value = 'http://' + url + '.local:8081/?action=snapshot'
snapshot_value = snapshot_value.replace('\n', '').replace(' ', '')
stream_value = 'http://' + url + '.local:8081/?action=stream'
stream_value = stream_value.replace('\n', '').replace(' ', '')

if 'webcam' not in octoprint_config_yaml:
    octoprint_config_yaml['webcam'] = {}

octoprint_config_yaml['webcam']['ffmpeg'] = ffpmeg_value
octoprint_config_yaml['webcam']['snapshot'] = snapshot_value
octoprint_config_yaml['webcam']['stream'] = stream_value

octoprint_config_data = open(octoprint_config_file_path, "w")
yaml.dump(octoprint_config_yaml, octoprint_config_data)
octoprint_config_data.close()
