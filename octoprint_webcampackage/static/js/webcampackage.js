ko.observable.fn.beforeAndAfterSubscribe = function (callback, target) {
  var _oldValue;
  this.subscribe(function (oldValue) {
    _oldValue = oldValue;
  }, null, 'beforeChange');

  this.subscribe(function (newValue) {
    callback.call(target, _oldValue, newValue);
  });
};

function WebcamViewModel(parameters) {
  var self = this;

  self.installed = ko.observable();
  self.isHubS = ko.observable();
  self.installedText = ko.computed(function () {
    if (self.isHubS()) {
      return self.installed() ? 'Installed' : 'Not Installed'
    } else {
      return 'Unavailable - this device is not a CANVAS Hub S'
    }
  });
  self.installStarted = ko.observable();
  self.installFinished = ko.observable();
  self.detailsText = ko.computed(function () {
    if (self.installStarted() && !self.installFinished()) {
      return 'This may take a while. You will be advised when the installation is completed.';
    } else if (self.installStarted() && self.installFinished()) {
      return 'Installation completed. Please insert your webcam and reboot your CANVAS Hub.';
    }
    return '';
  })

  self.selectedResolution = ko.observable();
  self.resolutions = ko.observable(webcamConstants.resolutions)
  self.frameRate = ko.observable();
  self.quality = ko.observable();
  self.yuv = ko.observable();
  self.saveConfigButtonText = ko.observable("Save Settings");
  self.saveConfigDisabled = ko.observable(false);
  self.manualConfig = ko.observable();
  self.configString = ko.computed(function () {
    if (self.manualConfig()) {
      return self.manualConfig().trim();
    } else {
      const resolution = self.selectedResolution() ? `-r ${self.selectedResolution()}` : '';
      const frameRate = self.frameRate() ? `-f ${self.frameRate()}` : '';
      const quality = self.quality() ? `-q ${self.quality()}` : '';
      const yuv = self.yuv() ? `-y` : '';
      const configString = `${resolution} ${frameRate} ${quality} ${yuv}`
      return configString.trim();
    }
  });

  self.installPackage = () => {
    const payload = { command: "installPackage" };
    self.ajaxRequest(payload);
  };

  self.saveConfigSettings = () => {
    const payload = {
      command: "saveConfigSettings",
      selectedResolution: self.selectedResolution(),
      frameRate: self.frameRate(),
      quality: self.quality(),
      yuv: self.yuv(),
      manualConfig: self.manualConfig()
    };
    self.ajaxRequest(payload);
  };

  self.frameRate.beforeAndAfterSubscribe(function (oldValue, newValue) {
    if (newValue >= 1 && newValue <= 120) {
      self.frameRate(parseInt(newValue));
    } else {
      self.frameRate(oldValue);
    }
  });

  self.quality.beforeAndAfterSubscribe(function (oldValue, newValue) {
    if (newValue >= 1 && newValue <= 100) {
      self.quality(parseInt(newValue));
    } else {
      self.quality(oldValue);
    }
  });

  self.listenForManualInputChange = () => {
    $('.manual-config-input').on('input', (event) => {
      self.manualConfig(event.target.value.trim());
    });
  }

  self.ajaxRequest = payload => {
    return $.ajax({
      url: API_BASEURL + "plugin/webcampackage",
      type: "POST",
      dataType: "json",
      data: JSON.stringify(payload),
      contentType: "application/json; charset=UTF-8"
    });
  };

  self.onBeforeBinding = () => {
    self.installStarted(false);
    self.installFinished(false);
    self.listenForManualInputChange();
  };

  self.onDataUpdaterPluginMessage = (pluginIdent, message) => {
    if (pluginIdent === "webcampackage") {
      if (message.command === "installationStart") {
        self.installStarted(true);
      } else if (message.command === "installationFinished") {
        self.installFinished(true);
      } else if (message.command === "installed") {
        self.installed(message.status);
      } else if (message.command === "isHubS") {
        self.isHubS(message.status);
      } else if (message.command === "selectedResolution") {
        self.selectedResolution(message.status)
      } else if (message.command === "frameRate") {
        self.frameRate(message.status);
      } else if (message.command === "quality") {
        self.quality(message.status);
      } else if (message.command === "yuv") {
        self.yuv(message.status);
      } else if (message.command === "manualConfig") {
        self.manualConfig(message.status);
      } else if (message.command === "settingsSaved") {
        self.saveConfigButtonText('Saved');
        self.saveConfigDisabled(true);
        const base_url = window.location.origin;
        window.location.href = `${base_url}/#control`;
        setTimeout(function () {
          self.saveConfigButtonText('Save Settings')
          self.saveConfigDisabled(false);
        }, 3000);
      }
    };
  }

}

/* ======================
  RUN
  ======================= */

$(function () {
  OCTOPRINT_VIEWMODELS.push({
    // This is the constructor to call for instantiating the plugin
    construct: WebcamViewModel,
    elements: ["#tab_plugin_webcampackage"]
  }); // This is a list of dependencies to inject into the plugin. The order will correspond to the "parameters" arguments above // Finally, this is the list of selectors for all elements we want this view model to be bound to.
});
