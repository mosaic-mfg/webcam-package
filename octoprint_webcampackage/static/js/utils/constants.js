const webcamConstants = {
  resolutions: [
    { value: "160x120", label: "160x120 (QQVGA)" },
    { value: "176x144", label: "176x144" },
    { value: "320x240", label: "320x240 (QVGA)" },
    { value: "352x288", label: "352x288" },
    { value: "424x240", label: "424x240" },
    { value: "432x240", label: "432x240" },
    { value: "640x360", label: "640x360" },
    { value: "640x480", label: "640x480 (VGA)" },
    { value: "800x448", label: "800x448" },
    { value: "800x600", label: "800x600 (SVGA)" },
    { value: "960x544", label: "960x544" },
    { value: "1280x720", label: "1280x720 (HD)" },
    { value: "1920x1080", label: "1920x1080 (FHD)" }
  ]
}