# WEBCAM PACKAGE PLUGIN

This OctoPrint plugin enables webcam functionality on a CANVAS Hub S

## Installing

On your OctoPrint server, manually install this plugin via the Plugin Manager using the following URL:

`https://gitlab.com/mosaic-mfg/webcam-package/-/archive/master/webcam-package.zip`

## Authors

[Mosaic Manufacturing Ltd.](https://www.mosaicmfg.com/)

## License

This project is licensed under Creative Commons Public Licenses - see the [LICENSE](https://gitlab.com/mosaic-mfg/webcam-package/blob/master/LICENSE) file for more details.
